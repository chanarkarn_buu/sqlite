/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.chanarkarn.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author A_R_T
 */
public class UpdateUser {
     public static void main(String args[]) {

        Connection conn = null;
        String dbName = "user.db";
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            System.out.println("Opened database successfully");

            //Update
            stmt = conn.createStatement();
            String sql = "UPDATE USER set USERNAME = WinWin where ID=1;";
            stmt.executeUpdate(sql);
            conn.commit();

            //Select
            ResultSet rs = stmt.executeQuery("SELECT * FROM USER;");

            while (rs.next()) {
                int id = rs.getInt("id");
                String username = rs.getString("username");
                int password = rs.getInt("password");

                System.out.println("ID = " + id);
                System.out.println("USERNAME = " + username);
                System.out.println("PASSWORD = " + password);

                System.out.println();
            }
            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }
        System.out.println("Operation done successfully");
    }

    
}
